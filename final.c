#include<unistd.h>
#include<errno.h>
#include<sys/wait.h>
#include<stdlib.h>
#include<signal.h>
#include<fcntl.h>
#include<sys/types.h>
#include<stdio.h>
#include<pwd.h>
#include<string.h>
#define bg 1
#define fg 2
int exit1=5;
int brun=1;
int frun=3,last=0;
int  cpid=-1;
int rdi[10000]={0};
int brsig=0,pid,emp=0,rear=-1,ristrm,rostrm,modein=0,modeout=0,lpipe[2],rpipe[2];int tin,tout,comm=0,inred=0,ored=0,infid,ofid;
int queue[1000000]={0},status[1000000]={0}; char name[100000][100];
void convert(int x,char c[])
{
	int cur=-1;
	char d[1000]={0};
	if(x==0)d[++cur]='0';
	while(x>0)
	{
		d[++cur]='0'+x%10;
		x/=10;
	}int i;
	for(i=0;i<=cur;i++)
		c[i]=d[cur-i];
	c[cur+1]='\0';
}
void wrt(char buff[])
{
	int x=strlen(buff);
	write(1,buff,x);
	return;
}

void handler(int num,siginfo_t* x, void*p)
{
	int pidc=x->si_pid;
	int i;
	//if(status1==SIGSTOP)return;
	for(i=0;i<=rear;i++){if(queue[i]==pidc)break;}
	if(x->si_status==SIGTSTP||x->si_status==SIGCONT) return;
	if(status[i]==frun){status[i]=exit1;return;}
	status[i]=exit1;
	char pidcc[1000],xc[1000],x2c[1000];
	convert(pidc,pidcc);
	convert(x->si_status,xc);
	convert(x->si_stime,x2c);
	wrt("\nprocess with pid ");wrt(pidcc); wrt(" exitted with status ");wrt(xc);wrt(".\nIt used "); wrt(x2c); wrt(" CPU time\n");
	brsig=1;
	fflush(stdout);
	return;
}
void spawnhandle(int num,siginfo_t* x, void*p)
{ 
	int pidc=x->si_pid;
	queue[rear]=pidc;
	return;
}
void init(char c[],char cwd[],char home[])
{
	getcwd(cwd,400);
	getcwd(home,400);
	gethostname(c,100);
}

void scn(char buff[])
{
	char c[2];int cnt=-1;
	while(read(0,c,1))
	{
		if(brsig==1)break;
		if(c[0]=='\n')break;
		else buff[++cnt]=c[0];
	}
	buff[++cnt]='\0';
	return;
}
void display(int file)
{
	int cnt=0;char c[2];
	while(read(file,c,1))
	{
		if(cnt<5||cnt==10||cnt==11)
			write(1,c,1);
		if(c[0]=='\n')cnt++;
	}
	return;
}
void tdetail(int sig)
{
	pid=getpid();
	char c[1000];
	convert(pid,c);
	wrt(c);
	wrt("\n");
}
void catch(int signum)
{
	wrt("caught\n");
}
void z(int signum)
{
	if(cpid!=-1)
	{
		kill(cpid,SIGTSTP);
	}
}
void overkill()
{
	int i;
	for(i=0;i<=rear;i++)
	{
		if(status[i]!=exit1)
		{
			kill(queue[i],SIGKILL);
			status[i]=exit1;
		}
	}
}
void execute(char*command[],int cnt)
{
	int mode=fg;
	if(cnt<=-1)return;
	if(strcmp(command[cnt],"&")==0)
	{
		mode=bg;
		command[cnt]=NULL;
	}else{
		mode=fg;
		command[++cnt]=NULL;
	}
	if(strcmp(command[0],"cd")==0)
	{
		if(chdir(command[1])<0)printf("No such directory\n");
	}else if(strcmp(command[0],"quit")==0)
	{
		signal(SIGCHLD,SIG_IGN);
		overkill();
		_exit(0);
	}else if(strcmp(command[0],"pinfo")==0)
	{
		int in;
		char target[10000];
		strcpy(target,"/proc/");
		char pchar[1000];
		convert(pid,pchar);
		if(cnt>1)
			strcat(target,command[1]);
		else
			strcat(target,pchar);
		strcat(target,"/status");
		strcat(target,"\0");
		in=open(target,O_RDONLY);
		if(in>=0)
			display(in);
		else
			wrt("no such process\n");
	}else if(strcmp(command[0],"jobs")==0)
	{
		int i;
		for(i=0;i<=rear;i++)
		{
			//				printf("%d\n",status[i]);
			if(status[i]!=exit1)
				printf("[%d]%s [%d]\n",i,name[i],queue[i]);
		}
	}else if(strcmp(command[0],"kjob")==0){
		if(cnt==3)
		{
			if(status[atoi(command[1])]!=exit1)
				kill(queue[atoi(command[1])],atoi(command[2]));
			else
				wrt("no such process\n");
		}else{
			wrt("wrong number of arguments\n");
		}
	}else if(strcmp(command[0],"fg")==0)
	{
		if(cnt==2)
		{
			int f=atoi(command[1]);
			if(status[f]!=exit1)
			{
				int*temp;
				cpid=queue[f];
				status[f]=frun;
				kill(queue[f],SIGCONT);
				sleep(1);
				mode=fg;
				waitpid(queue[f],temp,WUNTRACED);
				if(WIFSTOPPED(temp))
				{
					kill(queue[f],SIGCONT);
					wrt("+[z] pushed to background\n");
					int j;
					for(j=0;j<=rear;j++)
					{
						if(queue[j]==queue[f])
						{
							status[j]=brun;break;
						}
					}
					//	kill(pid1,SIGCONT);
				}
			}else{
				wrt("no such process\n");
			}
		}else{
			wrt("wrong number of arguments\n");
		}
		fflush(stdout);
		cpid=-1;
	}else if(strcmp(command[0],"overkill")==0)
	{
		overkill();
	}else
	{
		if(mode==fg)
			status[++rear]=frun;
		else
			status[++rear]=brun;
		strcpy(name[rear],command[0]);
		pipe(rpipe);
		//pipe(lpipe);
		int pid1=fork();
		if(pid1!=0){
			queue[rear]=pid1;
			cpid=pid1;
			if(mode==fg)
			{
				int status1;
				close(rpipe[1]);close(lpipe[0]);close(lpipe[1]);
				waitpid(pid1,&status1,WUNTRACED);
				mode=bg;
				if(WIFSTOPPED(status1))
				{
					kill(pid1,SIGCONT);
					wrt("+[z] pushed to background\n");
					int j;
					for(j=0;j<=rear;j++)
					{
						if(queue[j]==pid1)
						{
							status[j]=brun;
							break;
						}
					}
					//	kill(pid1,SIGCONT);
				}
			}
				dup2(tin,0);
				dup2(tout,1);
				lpipe[0]=rpipe[0];
				lpipe[1]=rpipe[1];
				fflush(stdout);
		}
		if(pid1==0){
			if(comm!=0)
				dup2(lpipe[0],0);
			else
				dup2(tin,0);
			if(last==1)
			{
				dup2(tout,1);
			}else{
				dup2(rpipe[1],1);
			}
			close(lpipe[0]);close(lpipe[1]);close(rpipe[0]);
			if(inred==1)
				dup2(infid,0);
			if(ored==1)
				dup2(ofid,1);
			int x=execvp(command[0],command);
			if(x==-1)
			{
				wrt(command[0]);
				wrt(":");
				perror("Error ");
				printf("Error Code : %d\n",errno);
			}
			_exit(0);
		}
		cpid=-1;
	}
	fflush(stdout);
}
void contract(char in[],char new[])
{
	int cur=0,ncnt=-1,len=strlen(in);
	while(cur<len)
	{
		while(in[cur]==' '||in[cur]=='\t'){cur++;if(cur>=len)break;}
		new[++ncnt]=' ';
		if(cur>=len)break;
		while(!(in[cur]==' '||in[cur]=='\t'))
		{
			if(in[cur]=='<'||in[cur]=='>'||in[cur]=='|')
			{
				new[++ncnt]=' ';
				new[++ncnt]=in[cur];
				cur++;
				break;
			}
			int i=0;
			new[++ncnt]=in[cur];
			cur++;
			if(cur>=len)break;
		}
	}
	new[++ncnt]='\0';
	return;
}

int main()
{
	tin=dup(0);
	tout=dup(1);
	pipe(lpipe);pipe(rpipe);
	signal(SIGINT,SIG_IGN);
	signal(SIGQUIT,SIG_IGN);
	signal(SIGTSTP,z);
	//	signal(SIGUSR1,tdetail);
	struct sigaction sigchld;
	sigchld.sa_sigaction=handler;
	sigchld.sa_flags=SA_SIGINFO;
	//spawn.sa_sigaction=spawnhandle;
	//spawn.sa_flags=SA_SIGINFO;
	sigaction(SIGCHLD,&sigchld,NULL);
	//sigaction(SIGUSR1,&spawn,NULL);
	int file,uid=getuid();
	pid=getpid();
	struct passwd *passwd;
	passwd=getpwuid(getuid());
	char c[100];
	char cwd[400];
	char home[400];
	char in[400];
	init(c,cwd,home);
	//use write command
	while(1)
	{
		fflush(stdout);
		getcwd(cwd,400);
		if(strcmp(home,cwd)==0)strcpy(cwd,"~");
		char term[10000]={0};
		strcpy(term,"<");strcat(term,passwd->pw_name);strcat(term,"@");strcat(term,c);strcat(term,":");strcat(term,cwd);strcat(term,">");wrt(term);
		char buff[10000]={0},in[10000]={0};
		char* command[1000]={0};
		scn(in);
		if(brsig==1){brsig=0;continue;}
	//	printf("contb\n");
		contract(in,buff);
	//	printf("conta\n");
	//	printf("%s\n",buff);
		char *split=NULL;
		split=strtok(buff," ");
		int cnt=-1;
		int segments=0,pipefd[1000];
		
		while(split!=NULL)
		{
			command[++cnt]=split;
			split=strtok(NULL," ");
		}
		if(cnt<=-1)continue;
		int i=0,len=-1;
		char* segment[1000]={0};
		pipe(lpipe);
		last=0;
		comm=0;
		inred=-1;ored=-1;
		while(i<=cnt)
		{
			if(strcmp(command[i],"<")==0){
				infid=open(command[i+1],O_RDONLY);
				if(infid<0)
				{
					perror("invalid redirection");
					break;
				}
				dup2(infid,0);
				close(infid);
				inred=1;
				i++;
			}
			if(strcmp(command[i],">")==0){
				ofid=open(command[i+1], O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
				if(ofid<0)
				{
					perror("invalid redirection");
					break;
				}
				dup2(ofid,1);
				ored=1;
				i++;
			}
			if(strcmp(command[i],"|")==0)
			{
				execute(segment,len);
				comm++;
				ored=-1;
				inred=-1;
				len=-1;
			}else if(i!=cnt){
				segment[++len]=command[i];
			}else{
				if(!(inred==1 || ored==1)){segment[++len]=command[i];}
				last=1;
				execute(segment,len);
				comm++;
				last=0;
				len=-1;
				inred=-1;
				ored=-1;
			}
			i++;
		}
		close(lpipe[0]);
		close(lpipe[1]);
		close(rpipe[0]);
		close(rpipe[1]);
		fflush(stdout);
		dup2(tin,0);
		dup2(tout,1);
		//for(m=0;m<=cnt;m++)printf("%s\n",command[m]);
	}
	return 0;
}
